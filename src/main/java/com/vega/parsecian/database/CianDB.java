package com.vega.parsecian.database;

import com.vega.parsecian.entity.Cian;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by Вася-Вега on 29.01.2017.
 */
public class CianDB {
    static Connection connection;

    public static void connect() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:cianDB.db");
    }

    public static void createDB() throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("CREATE TABLE if not exists 'cian'"
                + "('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'url' text, 'status' int, 'advertNum' int, 'balcony' text"
                + ", 'bathroom' text, 'flatRepairs' text, 'floor' int, 'numberOfStoreysInBuilding' int, 'latitude' real"
                + ", 'longitude' real, 'total' text, 'currency' text, 'price' text, 'region' text, 'rawAddress' text"
                + ", 'realtyType' text, 'text' text, 'lastUpdate' text, 'phone' text, 'photoUrls' text);");
        statement.close();
    }

    public static void writeDB(Cian cian) throws SQLException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        PreparedStatement statement = connection.prepareStatement(
                "insert into cian values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        statement.setString(2, cian.url);
        statement.setInt(3, cian.status);
        statement.setInt(4, cian.advertNum);
        statement.setString(5, cian.balcony);
        statement.setString(6, cian.bathroom);
        statement.setString(7, cian.flatRepairs);
        statement.setInt(8, cian.floor);
        statement.setInt(9, cian.numberOfStoreysInBuilding);
        statement.setDouble(10, cian.latitude);
        statement.setDouble(11, cian.longitude);
        statement.setString(12, cian.total);
        statement.setString(13, cian.currency);
        statement.setString(14, cian.price);
        statement.setString(15, cian.region);
        statement.setString(16, cian.rawAddress);
        statement.setString(17, cian.realtyType);
        statement.setString(18, cian.text);
        statement.setString(19, dateFormat.format(cian.lastUpdate));
        statement.setString(20, cian.phone);
        statement.setString(21, cian.photoUrls);

        statement.executeUpdate();
        statement.close();
    }

    public static void closeConnect() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
