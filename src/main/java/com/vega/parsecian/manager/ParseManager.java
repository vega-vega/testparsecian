package com.vega.parsecian.manager;

import com.vega.parsecian.entity.Cian;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Date;
import java.util.regex.Pattern;

public class ParseManager {
    static final int PARSE_STATUS_ERROR = 0;
    static final int PARSE_STATUS_SUCCESS = 1;
    static final int PARSE_STATUS_NOT_FULL = 2;

    static final String ELEMENT_TABLE_NAME_BALCONY = "Балкон:";
    static final String ELEMENT_TABLE_NAME_BATHROOM = "Раздельных санузлов:";
    static final String ELEMENT_TABLE_NAME_REPAIRS = "Ремонт:";
    static final String ELEMENT_TABLE_NAME_FLOOR = "Этаж:";
    static final String ELEMENT_TABLE_NAME_TOTAL = "Общая площадь:";
    static final String ELEMENT_TABLE_NAME_HOUSE = "Тип земли:";
    static final String ELEMENT_TABLE_NAME_APARTMENT = "Тип дома:";

    private static final String CIAN_URL = "https://www.cian.ru/sale/flat/";
    boolean fullParse;

    public Cian getParsedCian(int num) {
        return parseCian(num);
    }

    protected Cian parseCian(int advertNum) {
        String url = CIAN_URL + advertNum;
        fullParse = true;

        Cian cian = new Cian();
        cian.url = url;
        cian.advertNum = advertNum;
        cian.lastUpdate = new Date();

        try {
            Document document = Jsoup.connect(url).get();
            Element descrProps = document.select(".object_descr_props").first();

            cian.balcony = parseStringTableElement(descrProps, ELEMENT_TABLE_NAME_BALCONY);
            cian.bathroom = parseStringTableElement(descrProps, ELEMENT_TABLE_NAME_BATHROOM);
            cian.flatRepairs = parseStringTableElement(descrProps, ELEMENT_TABLE_NAME_REPAIRS);
            cian.floor = parseFloorTableElement(descrProps, true);
            cian.numberOfStoreysInBuilding = parseFloorTableElement(descrProps, false);
            Element map = document.select(".object_descr_map_static").first().select("input").first();
            cian.latitude = parseGeoMap(map, false);
            cian.longitude = parseGeoMap(map, true);
            cian.total = parseStringTableElement(descrProps, ELEMENT_TABLE_NAME_TOTAL);
            Element priceElement = document.select(".object_descr_price").first();
            cian.currency = parseStringPriceElement(priceElement, false);
            cian.price = parseStringPriceElement(priceElement, true);
            cian.region = document.select("h1.object_descr_addr").first().select("a").first().text();
            cian.rawAddress = parseAddressStringBreadcrumbsElement(document.select(".breadcrumbs.breadcrumbs_override").first());
            cian.realtyType = parseRealtyTypeStringTableElement(descrProps);
            cian.text = parseTextElement(document.select(".object_descr_text").first());
            cian.phone = parseStringElement(document.select(".cf_offer_show_phone-number").first());
            cian.photoUrls = parseStringImgSrcElements(document.select(".fotorama").first());

            cian.status = fullParse ? PARSE_STATUS_SUCCESS : PARSE_STATUS_NOT_FULL;
        } catch (IOException e) {
            cian.status = PARSE_STATUS_ERROR;
            e.printStackTrace();
        }

        return cian;
    }

    protected String parseStringTableElement(Element element, String name) {
        String s = null;

        if (element == null) {
            fullParse = false;
            return s;
        }

        Elements trElements = element.select("tr");

        for (Element tr : trElements) {
            String trName = tr.select("th").first().text();

            if (trName.equals(name)) {
                s = tr.select("td").first().text();
            }
        }

        if (s == null || s.isEmpty()) {
            fullParse = false;
        }

        return s;
    }

    /**
     * @param isFloor if this is true, return flor else return number of storeys.
     */
    protected int parseFloorTableElement(Element element, boolean isFloor) {
        int floor = 1;
        boolean fullParseBefore = fullParse;

        String value = parseStringTableElement(element, ELEMENT_TABLE_NAME_FLOOR);
        fullParse = fullParseBefore;

        if (value == null || value.isEmpty()) {
            return floor;
        }

        String[] array = value.split("/");
        if (array.length != 0) {
            String s = isFloor ? array[0] : array[1];
            floor = Integer.parseInt(s.replaceAll("\\D+", ""));
        }

        return floor;
    }

    /**
     * @param isPrice if this is true, return price else currency.
     */
    protected String parseStringPriceElement(Element priceElement, boolean isPrice) {
        String s = priceElement.text();

        if (s == null || s.isEmpty()) {
            fullParse = false;
            return s;
        }

        String[] array = s.trim().split(" ");
        if (array.length != 0) {
            if (isPrice) {
                s = s.replaceAll("\\D+", "");
            } else {
                Pattern p = Pattern.compile("[А-я]+");
                for (String value : array) {
                    s = p.matcher(value).find() ? value : "";
                }
            }
        }

        if (s == null || s.isEmpty()) {
            fullParse = false;
        }

        return s;
    }

    protected String parseAddressStringBreadcrumbsElement(Element element) {
        Elements elements = element.select("a.breadcrumbs__item");
        Element address = elements.get(elements.size() - 1);
        String s = address.text();

        if (s == null || s.isEmpty()) {
            fullParse = false;
        }

        return s;
    }

    protected String parseRealtyTypeStringTableElement(Element element) {
        boolean fullParseBefore = fullParse;

        String s = parseStringTableElement(element, ELEMENT_TABLE_NAME_APARTMENT);
        fullParse = fullParseBefore;

        if (s == null || s.isEmpty()) {
            s = parseStringTableElement(element, ELEMENT_TABLE_NAME_HOUSE);
        }

        if (s == null || s.isEmpty()) {
            fullParse = false;
        }

        return s;
    }

    protected String parseTextElement(Element element) {
        String s = element.text();
        String[] array = s.split("ID:");

        if (array.length == 0) {
            fullParse = false;
            return null;
        }

        s = array[0];
        return s;
    }

    protected String parseStringElement(Element element) {
        if (element == null) {
            fullParse = false;
            return null;
        }

        String s = element.text();

        if (s == null || s.isEmpty()) {
            fullParse = false;
        }

        return s;
    }

    protected String parseStringImgSrcElements(Element element) {
        String s = "";

        if (element == null) {
            return s;
        }

        for (Element children : element.children()) {
            s += children.absUrl("src") + ";";
        }

        return s;
    }

    protected double parseGeoMap(Element element, boolean isLong) {
        String s = element.val();
        String[] array = s.split("pt=");

        if (array.length == 0) {
            return 0;
        }

        array = array[1].split(",");
        if (array.length == 0) {
            return 0;
        }
        s = isLong ? array[1] : array[0];

        return Double.parseDouble(s);
    }
}
