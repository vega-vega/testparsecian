package com.vega.parsecian.entity;

import java.util.Date;

/**
 * Created by Вася-Вега on 29.01.2017.
 */
public class Cian {
    public String url;
    public int status;
    public int advertNum;
    public String balcony;
    public String bathroom;
    public String flatRepairs;
    public int floor;
    public int numberOfStoreysInBuilding;
    public double latitude;
    public double longitude;
    public String total;
    public String currency;
    public String price;
    public String region;
    public String rawAddress;
    public String realtyType;
    public String text;
    public Date lastUpdate;
    public String phone;
    public String photoUrls;
}
