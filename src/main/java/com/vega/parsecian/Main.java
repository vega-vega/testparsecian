package com.vega.parsecian;

import com.vega.parsecian.database.CianDB;
import com.vega.parsecian.entity.Cian;
import com.vega.parsecian.manager.ParseManager;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            return;
        }

        int start = Integer.parseInt(args[0]);
        int end = args.length == 2 ? Integer.parseInt(args[1]) : start;

        ParseManager parseManager = new ParseManager();

        try {
            CianDB.connect();
            CianDB.createDB();

            for (int i = start; i <= end; i++) {
                Cian cian = parseManager.getParsedCian(i);
                CianDB.writeDB(cian);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CianDB.closeConnect();
        }
    }
}
